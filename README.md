# Ansible xmlrpc\_client module

This was written mainly to handle one internal API where I work with [Ansible](https://www.ansible.com), but I assume it can handle any XMLRPC API where you call a certain method and provide positional arguments and/or keyword arguments.

# Documentation

## Installation

Either pop the file ``xmlrpc_client.py`` into ``./library/`` alongside your playbook, or put it in one of your library paths defined in ansible.cfg. As per [the docs](http://docs.ansible.com/ansible/latest/intro_configuration.html#library).

	library = /usr/share/ansible:/home/user/Ansible/library

## Description

    - This module lets you establish an XMLRPC session and call any method
      by its xmlrpc path. On dry run this module will only establish API
      connection and check if your method exists in the method list but not
      call it.

## Options

    url:
        description:
            - The API URL you connect to.
        required: true
    path:
        description:
            - The RPC path/method you are calling.
        required: true
    args:
        description:
            - A list of arguments for the RPC call. Placed first in any RPC call.
        required: false
    kwargs:
        description:
            - A dictionary of arguments for the RPC call. Comes after the args list.
        required: false

## Examples

```
---

vars:
  api_url: https://api.localhost:9002

tasks:
  # Establish session
  - name: login to api
    xmlrpc_client:
      url: "{{api_url}}"
      path: login
      args:
        - admin
        - secret password.
    register: sid

  # Use session to execute privileged command. In this case server.group.add
  # returns a group ID from the API.
  - name: create new server group
    xmlrpc_client:
      url: "{{api_url}}"
      path: server.group.add
      args:
        - "{{sid.returned}}"
        - Dev environment web server group - Sweden, Malmö
        - web
    register: server_group_id
    when: sid|changed

  # Use previous value in more API calls.
  - name: create new server
    xmlrpc_client:
        url: "{{api_url}}"
        path: server.add
        args:
            - "{{sid.returned}}"
            - vm-web01
            - 80
            - "{{server_group_id.returned}}"
    register: server_id
    when: server_group_id.returned
```

## Return
```
returned:
    description: The output value that the RPC call returns
    type: string
    returned: success, changed
```

# Author

Stefan Midjich (@stemid)
